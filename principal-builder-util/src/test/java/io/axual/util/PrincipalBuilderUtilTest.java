package io.axual.util;

/*-
 * ========================LICENSE_START=================================
 * Principal Builder Utilities
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import static io.axual.util.PrincipalBuilderUtil.buildAdvancedPrincipalChain;
import static io.axual.util.PrincipalBuilderUtil.buildBasicPrincipalChain;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class PrincipalBuilderUtilTest {

    private static final String TEST_JKS = "testing.jks";
    private static final String TEST_EMAIL_JKS = "testEmail.jks";

    private final InputStream testKeyStoreStream = getClass().getClassLoader()
            .getResourceAsStream(TEST_JKS);
    private final InputStream keyStoreWithEmailStream = getClass().getClassLoader()
            .getResourceAsStream(TEST_EMAIL_JKS);

    /**
     * The emails get wrapped in an ASN.1 structure. This is expected. It is recommended not to use
     * emails in the DN.
     */
    private static final String EXPECTED_TEST_CHAIN_ADVANCED = "[0] CN=Axual Dummy Intermediate 2018 01, [1] CN=test CN,OU=Axual,O=Axual,L=Utrecht,ST=Utrecht,C=NL";
    private static final String EXPECTED_EMAIL_CHAIN_ADVANCED = "[0] CN=test-with-email,OU=Axual,O=Axual,L=Utrecht,ST=Utrecht,C=NL,1.2.840.113549.1.9.1=#160e7465737440617875616c2e636f6d";

    private static final String EXPECTED_TEST_CHAIN_BASIC = "CN=test CN,OU=Axual,O=Axual,L=Utrecht,ST=Utrecht,C=NL";
    private static final String EXPECTED_EMAIL_CHAIN_BASIC = "CN=test-with-email,OU=Axual,O=Axual,L=Utrecht,ST=Utrecht,C=NL,1.2.840.113549.1.9.1=#160e7465737440617875616c2e636f6d";


    private Certificate[] extractChain(final InputStream keyStoreStream) {
        ArrayList<Certificate> certificateList = new ArrayList<>();

        try {
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(keyStoreStream, "notsecret".toCharArray());
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
            trustManagerFactory.init(keyStore);
            TrustManager[] tm = trustManagerFactory.getTrustManagers();
            // Initialize SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, tm, null);

            Enumeration<String> aliases = keyStore.aliases();

            while (aliases.hasMoreElements()) {
                certificateList.addAll(Arrays.asList(keyStore.getCertificateChain(aliases.nextElement())));
            }
        } catch (Exception ex) {
            return new Certificate[0];
        }

        return certificateList.toArray(new Certificate[0]);
    }

    @Test
    void testBuildAdvancedPrincipalChain() {
        Certificate[] chain = extractChain(testKeyStoreStream);
        String advancedPrincipalChain = buildAdvancedPrincipalChain(chain);

        assertEquals(EXPECTED_TEST_CHAIN_ADVANCED, advancedPrincipalChain);
    }

    @Test
    void testBuildAdvancedPrincipalChainEmail() {
        Certificate[] chain = extractChain(keyStoreWithEmailStream);
        String advancedPrincipalChain = buildAdvancedPrincipalChain(chain);

        assertEquals(EXPECTED_EMAIL_CHAIN_ADVANCED, advancedPrincipalChain);
    }

    @Test
    void testBuildBasicPrincipalChain() {
        Certificate[] chain = extractChain(testKeyStoreStream);
        String advancedPrincipalChain = buildBasicPrincipalChain(chain);

        assertEquals(EXPECTED_TEST_CHAIN_BASIC, advancedPrincipalChain);
    }

    @Test
    void testBuildBasicPrincipalChainEmail() {
        Certificate[] chain = extractChain(keyStoreWithEmailStream);
        String advancedPrincipalChain = buildBasicPrincipalChain(chain);

        assertEquals(EXPECTED_EMAIL_CHAIN_BASIC, advancedPrincipalChain);
    }

}
