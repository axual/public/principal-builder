package io.axual.util;

/*-
 * ========================LICENSE_START=================================
 * Principal Builder Utilities
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static javax.security.auth.x500.X500Principal.RFC2253;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import javax.security.auth.x500.X500Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrincipalBuilderUtil {

  private static final Logger log = LoggerFactory.getLogger(PrincipalBuilderUtil.class);

  private PrincipalBuilderUtil() {
    throw new IllegalStateException("Utility class");
  }

  public static String buildAdvancedPrincipalChain(Certificate[] chain) {
    List<String> principals = new ArrayList<>();
    for (int index = chain.length - 1; index >= 0; index--) {
      X509Certificate cert = (X509Certificate) chain[index];
      X500Principal principal = cert.getSubjectX500Principal();
      X500Principal issuerPrincipal = cert.getIssuerX500Principal();
      if (index == chain.length - 1 && !principal.equals(issuerPrincipal)) {
        // Add issuer for top entry if not already passed in the chain
        principals.add(issuerPrincipal.getName(RFC2253));
      }
      principals.add(principal.getName(RFC2253));
    }
    return concatPrincipals(principals);
  }

  public static String buildBasicPrincipalChain(Certificate[] chain) {
    X509Certificate cert = (X509Certificate) chain[0];
    X500Principal principal = cert.getSubjectX500Principal();
    return principal.getName(RFC2253);
  }

  public static String concatPrincipals(List<String> subjects) {
    String principal = "";
    for (int index = 0; index < subjects.size(); index++) {
      principal = appendEntry(principal, subjects.get(index), index);
    }
    log.debug("SSLSession principal : {}", principal);
    return principal;
  }

  public static String appendEntry(String entries, String entry, int index) {
    if (!entries.isEmpty()) {
      entries += ", ";
    }
    return entries + "[" + index + "] " + entry;
  }
}
