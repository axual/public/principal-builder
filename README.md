# Axual Principal Builder


[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![pipeline status](https://gitlab.com/axual-public/principal-builder/badges/master/pipeline.svg)](https://gitlab.com/axual-public/principal-builder/commits/master) 
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-principal-builder&metric=alert_status&token=22bad794a18b0867ecb5c3b781ffba42f9fe2dce)](https://sonarcloud.io/dashboard?id=axual-public-principal-builder)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-principal-builder&metric=coverage&token=22bad794a18b0867ecb5c3b781ffba42f9fe2dce)](https://sonarcloud.io/dashboard?id=axual-public-principal-builder)

## Shared utility library for ACL Principal formatting.

## Modules

### acl-principal-builder
Contains the abstractions for ACL Principal building to be used by components constructing principals for remote validation

### principal-builder-application
Contains the Kafka Principal Builder used by the Kafka broker to construct the principal names

### principal-builder-util
Contains the helpers to build the actual principals

## License
Configuration Provider for HashiCorp Vault is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).

## Contributing
Axual is interested in building the community; we would welcome any thoughts or 
[patches](https://gitlab.com/axual-public/principal-builder/-/issues).
You can reach us [here](https://axual.com/contact/).

See [contributing](https://gitlab.com/axual-public/principal-builder/blob/master/CONTRIBUTING.md).
  