package io.axual.security.principal;

/*-
 * ========================LICENSE_START=================================
 * ACL Principal Builder
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.util.PrincipalBuilderUtil.buildBasicPrincipalChain;

import java.security.cert.Certificate;

/**
 * Kafka broker default
 */
public class BasicAclPrincipalBuilder implements AclPrincipalBuilder {

    public BasicAclPrincipalBuilder() {
        // Explicit default constructor
    }

    @Override
    public String createAcl(Certificate[] chain) {
        return buildBasicPrincipalChain(chain);
    }

}
