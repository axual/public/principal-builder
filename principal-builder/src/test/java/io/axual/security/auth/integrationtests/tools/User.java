package io.axual.security.auth.integrationtests.tools;

/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

public class User {
    public static final User ADMIN_PLAIN = saslPlainUser("admin", "verysecretpassword");
    public static final User USER1_SCRAM = saslScramUser("user1", "anothersecretpassword");
    public static final User USER2_SCRAM = saslScramUser("user2", "anothersecretpassword");
    public static final User USER3_SSL = sslUser("[0] CN=Root CA for Software Development,OU=Axual Product Development,O=Axual B.V.,L=Utrecht,ST=Utrecht,C=NL, [1] CN=Kafka Compose Client 1,OU=Axual Product Development,O=Axual B.V.,ST=Utrecht,C=NL", 
            getResourcePath("integrationtests/client-1.keystore.jks"),
            "notsecret",
            "notsecret",
            getResourcePath("integrationtests/client.truststore.jks"),
            "notsecret");

    private final String username;
    private final String password;
    private final AuthenticationType authenticationType;
    private final String keystoreLocation;
    private final String keystorePassword;
    private final String keyPassword;
    private final String truststoreLocation;
    private final String truststorePassword;

    private User(String username, String password, AuthenticationType channel, String keystoreLocation, String keystorePassword, String keyPassword, String truststoreLocation, String truststorePassword) {
        this.username = username;
        this.password = password;
        this.authenticationType = channel;
        this.keystoreLocation = keystoreLocation;
        this.keystorePassword = keystorePassword;
        this.keyPassword = keyPassword;
        this.truststoreLocation = truststoreLocation;
        this.truststorePassword = truststorePassword;
    }

    public static User saslPlainUser(String username, String password) {
        return new User(username, password, AuthenticationType.PLAIN, null, null, null, null, null);
    }

    public static User saslScramUser(String username, String password) {
        return new User(username, password, AuthenticationType.SCRAM, null, null, null, null, null);
    }

    public static User sslUser(String username, String keystoreLocation, String keystorePassword, String keyPassword, String truststoreLocation, String truststorePassword) {
        return new User(username, null, AuthenticationType.SSL, keystoreLocation, keystorePassword, keyPassword, truststoreLocation, truststorePassword);
    }

    private static String getResourcePath(String resource) {
        Objects.requireNonNull(resource);
        URL location = User.class.getClassLoader().getResource(resource);
        if (location == null) {
            throw new RuntimeException("Could not find resource " + resource);
        }
        try {
            return new File(location.toURI()).getPath();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Could not create resource path for resource " + resource, e);
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }

    public String getKeystoreLocation() {
        return keystoreLocation;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public String getKeyPassword() {
        return keyPassword;
    }

    public String getTruststoreLocation() {
        return truststoreLocation;
    }

    public String getTruststorePassword() {
        return truststorePassword;
    }
}
