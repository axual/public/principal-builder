package io.axual.security.auth.integrationtests.tools.jupiter;

/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */




import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.axual.security.auth.integrationtests.tools.AuthenticationType;
import io.axual.security.auth.integrationtests.tools.KafkaTestCluster;
import io.axual.security.auth.integrationtests.tools.PortAllocator;
import io.axual.security.auth.integrationtests.tools.User;
import io.axual.security.auth.integrationtests.tools.impl.KafkaTestClusterImpl;

public class KafkaExtension implements KafkaTestCluster, BeforeEachCallback, AfterEachCallback, ExtensionContext.Store.CloseableResource {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaExtension.class);
    private final KafkaTestClusterImpl testCluster;
    private final List<Integer> allocatedPorts = new ArrayList<>();
    private String currentTest = null;
    private Optional<Logger> currentLogger = Optional.empty();

    public KafkaExtension() {
        allocatedPorts.addAll(PortAllocator.findAndAllocatePorts(4));
        testCluster = new KafkaTestClusterImpl(allocatedPorts.get(0), allocatedPorts.get(1), allocatedPorts.get(2), allocatedPorts.get(3));
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        currentLogger = Optional.ofNullable(LoggerFactory.getLogger(context.getRequiredTestClass()));
        currentTest = context.getDisplayName();
        currentLogger.orElse(LOG).info("Starting Connect test server for {}", currentTest);
        testCluster.start();
    }

    @Override
    public void afterEach(ExtensionContext context) {
        currentLogger = Optional.ofNullable(LoggerFactory.getLogger(context.getRequiredTestClass()));
        currentTest = context.getDisplayName();
        currentLogger.orElse(LOG).info("Stopping Connect test server for {}", currentTest);
        testCluster.stop();
    }

    @Override
    public void createTopic(String name, int partitions, boolean compacted) {
        testCluster.createTopic(name, partitions, compacted);
    }

    @Override
    public void deleteTopic(String name) {
        testCluster.deleteTopic(name);
    }

    @Override
    public String bootstrapServers(AuthenticationType channel) {
        return testCluster.bootstrapServers(channel);
    }

    @Override
    public Map<String, Object> clientConnectionProperties(User user) {
        return testCluster.clientConnectionProperties(user);
    }

    @Override
    public Map<String, Object> adminProperties(User user) {
        return testCluster.adminProperties(user);
    }

    @Override
    public boolean makeTopicVisible(User user, String topic) {
        return testCluster.makeTopicVisible(user, topic);
    }

    @Override
    public void close() {
        testCluster.close();
        PortAllocator.deallocatePorts(allocatedPorts);
        allocatedPorts.clear();
    }
}
