package io.axual.security.auth.integrationtests;/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.common.utils.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Set;
import java.util.stream.Stream;

import io.axual.security.auth.integrationtests.tools.User;
import io.axual.security.auth.integrationtests.tools.jupiter.KafkaExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * A real Kafka Server is started with the SslPrincipalBuilder class
 */
class SslPrincipalBuilderIT {

    @RegisterExtension
    KafkaExtension cluster = new KafkaExtension();

    
    @ParameterizedTest
    @MethodSource(value = "testPrincipalVisibility")
    void testPrincipalVisibility(final String topic, final User userCanSee, final User userCanNotSee) throws Exception{
        cluster.createTopic(topic);
        
        try (AdminClient client = AdminClient.create(cluster.adminProperties(userCanSee))) {
            Set<String> topics = client.listTopics().names().get();
            boolean topicSeenBefore = topics.contains(topic);
            assertFalse(topicSeenBefore, "Should not be able to see it but could see topic " + topic);
        }
        
        try (AdminClient client = AdminClient.create(cluster.adminProperties(userCanNotSee))) {
            Set<String> topics = client.listTopics().names().get();
            boolean topicSeenBefore = topics.contains(topic);
            assertFalse(topicSeenBefore, "Should not be able to see it but could see topic " + topic);
        }
        
        cluster.makeTopicVisible(userCanSee, topic);
        // Wait for ACL apply to be processed
        Utils.sleep(200);
        try (AdminClient client = AdminClient.create(cluster.adminProperties(userCanSee))) {
            Set<String> topics = client.listTopics().names().get();
            boolean topicSeenAfter = topics.contains(topic);
            assertTrue(topicSeenAfter, "Could not see topic " + topic);
        }

        try (AdminClient client = AdminClient.create(cluster.adminProperties(userCanNotSee))) {
            Set<String> topics = client.listTopics().names().get();
            boolean topicSeenAfter = topics.contains(topic);
            assertFalse(topicSeenAfter, "User2 could see topic " + topic);
        }
    }
    
    static Stream<Arguments> testPrincipalVisibility(){
        return Stream.of(
                Arguments.of( "seen-by-ssl", User.USER3_SSL, User.USER2_SCRAM),
                Arguments.of( "seen-by-scram", User.USER1_SCRAM, User.USER2_SCRAM)
        );
    }
}
