package io.axual.security.auth;

/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.security.auth.AuthenticationContext;
import org.apache.kafka.common.security.auth.KafkaPrincipal;
import org.apache.kafka.common.security.auth.PlaintextAuthenticationContext;
import org.apache.kafka.common.security.auth.SaslAuthenticationContext;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.security.auth.SslAuthenticationContext;
import org.apache.kafka.common.security.kerberos.KerberosShortNamer;
import org.apache.kafka.common.security.scram.internals.ScramMechanism;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.security.auth.x500.X500Principal;
import javax.security.sasl.SaslServer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SslPrincipalBuilderTest {

    @Mock
    private SslAuthenticationContext authenticationContext;

    @Mock
    private AuthenticationContext context;

    @Mock
    private SaslAuthenticationContext saslAuthenticationContext;

    @Mock
    private PlaintextAuthenticationContext plaintextAuthenticationContext;

    @Mock
    private SSLSession sslSession;

    private String ROOT_CA = "CN=Root CA";

    private String ROOT_CA_WITH_COMMA = "CN=Root\\, CA";

    private String INTERMEDIATE_CA = "CN=Intermediate CA 2014 01, DC=test, DC=corp";

    private String CERT = "CN=test, OU=Axual B.V., O=Axual B.V., L=Utrecht, ST=Utrecht, C=NL";

    private X500Principal rootPrincipal = new X500Principal(ROOT_CA);
    private X500Principal rootPrincipalWithComma = new X500Principal(ROOT_CA_WITH_COMMA);
    private X500Principal intermediatePrincipal = new X500Principal(INTERMEDIATE_CA);
    private X500Principal subjectPrincipal = new X500Principal(CERT);

    @InjectMocks
    private SslPrincipalBuilder sslPrincipalBuilder;


    @Test
    void verify_build_principal() throws SSLPeerUnverifiedException {
        X509Certificate chain = mock(X509Certificate.class);
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenReturn(new X509Certificate[]{chain, chain, chain});
        when(chain.getSubjectX500Principal())
                .thenReturn(rootPrincipal, intermediatePrincipal, subjectPrincipal);
        when(chain.getIssuerX500Principal()).thenReturn(rootPrincipal);
        KafkaPrincipal fullyQualifiedName = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(
                "[0] CN=Root CA, [1] CN=Intermediate CA 2014 01,DC=test,DC=corp, [2] CN=test,OU=Axual B.V.,O=Axual B.V.,L=Utrecht,ST=Utrecht,C=NL",
                fullyQualifiedName.getName());

    }

    @Test
    void verify_build_principal_with_comma_in_dn() throws SSLPeerUnverifiedException {
        X509Certificate chain = mock(X509Certificate.class);
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenReturn(new X509Certificate[]{chain, chain, chain});
        when(chain.getSubjectX500Principal())
                .thenReturn(rootPrincipalWithComma, intermediatePrincipal, subjectPrincipal);
        when(chain.getIssuerX500Principal()).thenReturn(rootPrincipalWithComma);
        KafkaPrincipal fullyQualifiedName = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(
                "[0] CN=Root\\, CA, [1] CN=Intermediate CA 2014 01,DC=test,DC=corp, [2] CN=test,OU=Axual B.V.,O=Axual B.V.,L=Utrecht,ST=Utrecht,C=NL",
                fullyQualifiedName.getName());

    }

    @Test
    void verify_build_principal_incomplete_chain() throws SSLPeerUnverifiedException {
        X509Certificate chain = mock(X509Certificate.class);
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenReturn(new X509Certificate[]{chain, chain});
        when(chain.getSubjectX500Principal()).thenReturn(intermediatePrincipal, subjectPrincipal);
        when(chain.getIssuerX500Principal()).thenReturn(rootPrincipal);
        KafkaPrincipal fullyQualifiedName = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(
                "[0] CN=Root CA, [1] CN=Intermediate CA 2014 01,DC=test,DC=corp, [2] CN=test,OU=Axual B.V.,O=Axual B.V.,L=Utrecht,ST=Utrecht,C=NL",
                fullyQualifiedName.getName());

    }

    @Test
    void verify_build_principal_single_level_chain() throws SSLPeerUnverifiedException {
        X509Certificate chain = mock(X509Certificate.class);
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenReturn(new X509Certificate[]{chain});
        when(chain.getSubjectX500Principal()).thenReturn(subjectPrincipal);
        when(chain.getIssuerX500Principal()).thenReturn(intermediatePrincipal);
        KafkaPrincipal fullyQualifiedName = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(
                "[0] CN=Intermediate CA 2014 01,DC=test,DC=corp, [1] CN=test,OU=Axual B.V.,O=Axual B.V.,L=Utrecht,ST=Utrecht,C=NL",
                fullyQualifiedName.getName());

    }

    @Test
    void verify_returns_anonymous_on_exception() throws SSLPeerUnverifiedException {
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenThrow(SSLPeerUnverifiedException.class);
        KafkaPrincipal fullyQualifiedPrincipal = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(KafkaPrincipal.ANONYMOUS, fullyQualifiedPrincipal);
    }

    @Test
    void verify_returns_anonymous_when_chain_length_0() throws SSLPeerUnverifiedException {
        when(authenticationContext.session()).thenReturn(sslSession);
        when(sslSession.getPeerCertificates()).thenReturn(new X509Certificate[]{});
        KafkaPrincipal fullyQualifiedPrincipal = sslPrincipalBuilder.build(authenticationContext);
        assertEquals(KafkaPrincipal.ANONYMOUS, fullyQualifiedPrincipal);
    }

    @Test
    void verify_principal_builder_SASL_scram() throws Exception {
        SaslServer server = mock(SaslServer.class);
        String principalName = "foo";

        when(server.getMechanismName()).thenReturn(ScramMechanism.SCRAM_SHA_256.mechanismName());
        when(server.getAuthorizationID()).thenReturn(principalName);

        KafkaPrincipal principal = sslPrincipalBuilder.build(new SaslAuthenticationContext(server,
                SecurityProtocol.SASL_PLAINTEXT, InetAddress.getLocalHost(), SecurityProtocol.SASL_PLAINTEXT.name()));
        assertEquals(KafkaPrincipal.USER_TYPE, principal.getPrincipalType());
        assertEquals(principalName, principal.getName());


        verify(server, atLeastOnce()).getMechanismName();
        verify(server, atLeastOnce()).getAuthorizationID();
    }

    @Test
    void verify_principal_builder_SASL_Gssapi() throws UnknownHostException {
        SaslServer server = mock(SaslServer.class);
        KerberosShortNamer kerberosShortNamer = mock(KerberosShortNamer.class);

        when(server.getMechanismName()).thenReturn(SaslConfigs.GSSAPI_MECHANISM);

        SaslAuthenticationContext authenticationContext =
                new SaslAuthenticationContext(server,
                        SecurityProtocol.SASL_PLAINTEXT,
                        InetAddress.getLocalHost(),
                        SecurityProtocol.SASL_PLAINTEXT.name());
        assertThrows(IllegalArgumentException.class,
                () -> sslPrincipalBuilder.build(authenticationContext));
    }

    @Test
    void verify_returns_anonymous_when_Plaintext() {
        KafkaPrincipal fullyQualifiedPrincipal = sslPrincipalBuilder
                .build(plaintextAuthenticationContext);
        assertEquals(KafkaPrincipal.ANONYMOUS, fullyQualifiedPrincipal);
    }

    @Test
    void throws_exception_unknown_context() {
        assertThrows(IllegalArgumentException.class,
                () -> sslPrincipalBuilder.build(context));
    }

    @Test
    void verify_principal_builder_Serde() throws Exception {
        SaslServer server = mock(SaslServer.class);
        String principalName = "foo";

        when(server.getMechanismName()).thenReturn(ScramMechanism.SCRAM_SHA_256.mechanismName());
        when(server.getAuthorizationID()).thenReturn(principalName);

        KafkaPrincipal principal = sslPrincipalBuilder.build(new SaslAuthenticationContext(server,
                SecurityProtocol.SASL_PLAINTEXT, InetAddress.getLocalHost(), SecurityProtocol.SASL_PLAINTEXT.name()));


        byte[] serializedPrincipal = sslPrincipalBuilder.serialize(principal);
        KafkaPrincipal deserializedPrincipal = sslPrincipalBuilder.deserialize(serializedPrincipal);
        assertEquals(principal, deserializedPrincipal);

        verify(server, atLeastOnce()).getMechanismName();
        verify(server, atLeastOnce()).getAuthorizationID();
    }
}
