package io.axual.security.auth.integrationtests.tools.impl;

/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import org.apache.commons.io.FileUtils;
import org.apache.curator.test.InstanceSpec;
import org.apache.curator.test.TestingServer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.acl.AccessControlEntry;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclOperation;
import org.apache.kafka.common.acl.AclPermissionType;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.errors.TopicExistsException;
import org.apache.kafka.common.resource.PatternType;
import org.apache.kafka.common.resource.ResourcePattern;
import org.apache.kafka.common.resource.ResourceType;
import org.apache.kafka.common.security.auth.AuthenticateCallbackHandler;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.security.scram.ScramCredential;
import org.apache.kafka.common.security.scram.ScramCredentialCallback;
import org.apache.kafka.common.security.scram.internals.ScramFormatter;
import org.apache.kafka.common.security.scram.internals.ScramMechanism;
import org.apache.kafka.common.utils.Time;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.AppConfigurationEntry;

import io.axual.security.auth.SslPrincipalBuilder;
import io.axual.security.auth.integrationtests.tools.AuthenticationType;
import io.axual.security.auth.integrationtests.tools.KafkaTestCluster;
import io.axual.security.auth.integrationtests.tools.User;
import kafka.metrics.KafkaMetricsReporter;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServer;
import kafka.utils.VerifiableProperties;
import scala.Option;
import scala.collection.Seq;

import static java.lang.String.format;

public class KafkaTestClusterImpl implements KafkaTestCluster, AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaTestClusterImpl.class);
    private static final short REPLICATION_FACTOR = 1;
    private static final AtomicInteger BROKER_ID_COUNTER = new AtomicInteger(1);


    private final int brokerPortAdmin;
    private final int brokerPortClientSasl;
    private final int brokerPortClientSsl;
    private final int zookeeperPort;
    private final int clusterId;
    private final User adminUser;
    private final Collection<User> users;
    private final AtomicBoolean isRunning = new AtomicBoolean(false);
    private TestingServer zookeeper;
    private KafkaServer broker;
    private File logDir;

    public KafkaTestClusterImpl(int zookeeperPort, int brokerPort, int brokerPortClientSsl, int brokerPortClientSasl) {
        this(zookeeperPort, brokerPort, brokerPortClientSsl, brokerPortClientSasl, User.ADMIN_PLAIN, User.USER1_SCRAM, User.USER2_SCRAM, User.USER3_SSL);
    }

    public KafkaTestClusterImpl(int zookeeperPort, int brokerPort, int brokerPortClientSsl, int brokerPortClientSasl, User adminUser, User... otherUsers) {
        clusterId = BROKER_ID_COUNTER.getAndIncrement();
        LOG.info("Creating KafkaTestCluster {}", clusterId);
        this.zookeeperPort = zookeeperPort;
        this.brokerPortAdmin = brokerPort;
        this.brokerPortClientSsl = brokerPortClientSsl;
        this.brokerPortClientSasl = brokerPortClientSasl;
        this.adminUser = adminUser;
        this.users = otherUsers == null ? Collections.emptyList() : Arrays.asList(otherUsers);
        zookeeper = null;
        broker = null;
    }

    public void start() {
        final ClassLoader originalContext = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(KafkaServer.class.getClassLoader());
            LOG.info("Starting KafkaTestCluster {}", clusterId);
            // Start zookeeper
            System.setProperty("zookeeper.serverCnxnFactory",
                    "org.apache.zookeeper.server.NettyServerCnxnFactory");
            LOG.info("Starting ZooKeeper on port {}", zookeeperPort);
            InstanceSpec spec = new InstanceSpec(null, zookeeperPort, -1, -1, true, -1, 2000, 10);
            zookeeper = new TestingServer(spec, true);

            // Start broker
            LOG.info("Starting Kafka {} on port {}", clusterId, brokerPortAdmin);
            try {
                logDir = Files.createTempDirectory("kafka").toFile();
            } catch (IOException e) {
                throw new RuntimeException("Unable to start Kafka", e);
            }

            Option<String> threadPrefix = Option.apply("kafka-tester-" + clusterId);
            KafkaConfig kafkaConfig = new KafkaConfig(brokerProperties());
            Properties reporterProperties = new Properties();
            reporterProperties.putAll(kafkaConfig.originals());


//            Seq<KafkaMetricsReporter> metricsReporters = KafkaMetricsReporter.startReporters(new VerifiableProperties(reporterProperties));
            broker = new KafkaServer(kafkaConfig, Time.SYSTEM, threadPrefix, false); // Used for Kafka 2.6
            broker.startup();

            isRunning.set(true);
            LOG.info("KafkaTestCluster {} Started", clusterId);
        } catch (Exception e) {
            throw new RuntimeException("Unable to start KafkaTestCluster", e);
        } finally {
            Thread.currentThread().setContextClassLoader(originalContext);
        }
    }

    public void stop() {
        try {
            LOG.info("Stopping KafkaTestCluster {}", clusterId);
            if (broker != null) {
                LOG.info("Shutting down Kafka for KafkaTestCluster {} on port {}", clusterId, brokerPortAdmin);
                broker.shutdown();
                broker.awaitShutdown();
                try {
                    FileUtils.cleanDirectory(logDir);
                } catch (IOException e) {
                    LOG.warn("Could not clean the directory {} for KafkaTestCluster {}", logDir.getAbsolutePath(), clusterId);
                }
                try {
                    Files.delete(logDir.toPath());
                } catch (IOException e) {
                    LOG.error("Error deleting the directory {} for KafkaTestCluster {}", logDir.getAbsolutePath(), clusterId);
                }
                broker = null;
            }
            if (zookeeper != null) {
                zookeeper.stop();
                zookeeper.close();
                zookeeper = null;
            }
            isRunning.set(false);
            LOG.info("KafkaTestCluster {} Stopped", clusterId);
        } catch (Exception e) {
            throw new RuntimeException("Unable to stop KafkaTestCluster " + clusterId, e);
        }
    }

    public boolean isRunning() {
        return isRunning.get();
    }

    @Override
    public void close() {
        LOG.info("Closing KafkaTestCluster {}", clusterId);
        stop();
        LOG.info("KafkaTestCluster Stopped {}", clusterId);
    }

    // using Sasl and
    private Map<String, Object> brokerProperties() {
        Map<String, Object> brokerProperties = new HashMap<>();
        brokerProperties.put("zookeeper.connect", zookeeper.getConnectString() + "/cluster_" + clusterId);
        brokerProperties.put("broker.id", Integer.toString(clusterId));
        brokerProperties.put("log.dir", logDir.getAbsolutePath());
        brokerProperties.put("log.flush.interval.messages", String.valueOf(1));
        brokerProperties.put("offsets.topic.replication.factor", "1");
        brokerProperties.put("min.insync.replicas", "1");
        brokerProperties.put("transaction.state.log.min.isr", "1");
        brokerProperties.put("transaction.state.log.replication.factor", "1");
        brokerProperties.put("auto.create.topics.enable", "false");
        brokerProperties.put("log.segment.delete.delay.ms", "100");
        brokerProperties.put("delete.topic.enable", "true");
        brokerProperties.put("allow.everyone.if.no.acl.found", "false");
//        brokerProperties.put("authorizer.class.name", "kafka.security.auth.SimpleAclAuthorizer");
        brokerProperties.put("authorizer.class.name", "kafka.security.authorizer.AclAuthorizer");
        brokerProperties.put("principal.builder.class", SslPrincipalBuilder.class.getName());
        brokerProperties.put("listener.security.protocol.map", "ADMIN:SASL_PLAINTEXT,CLIENTSSL:SSL,CLIENTSASL:SASL_PLAINTEXT");
        brokerProperties.put("listeners", format("ADMIN://localhost:%d,CLIENTSSL://localhost:%d,CLIENTSASL://localhost:%d", brokerPortAdmin, brokerPortClientSsl, brokerPortClientSasl));
        brokerProperties.put("advertised.listeners", format("ADMIN://localhost:%d,CLIENTSSL://localhost:%d,CLIENTSASL://localhost:%d", brokerPortAdmin, brokerPortClientSsl, brokerPortClientSasl));
        brokerProperties.put("mechanism.inter.broker.protocol", "SASL");
        brokerProperties.put("inter.broker.listener.name", "ADMIN");
        brokerProperties.put("sasl.mechanism.inter.broker.protocol", "PLAIN");
        // Client mTLS channel SSL settings for CLIENTSSL
        brokerProperties.put("listener.name.clientssl.ssl.keystore.location", getResourcePath("integrationtests/server.keystore.jks"));
        brokerProperties.put("listener.name.clientssl.ssl.keystore.password", "keystore_kafka-broker");
        brokerProperties.put("listener.name.clientssl.ssl.key.password", "keystore_kafka-broker");
        brokerProperties.put("listener.name.clientssl.ssl.truststore.location", getResourcePath("integrationtests/server.truststore.jks"));
        brokerProperties.put("listener.name.clientssl.ssl.truststore.password", "notsecret");
        brokerProperties.put("listener.name.clientssl.ssl.endpoint.identification.algorithm", " ");
        brokerProperties.put("listener.name.clientssl.ssl.client.auth", "required");
        // Admin SASL channel
        brokerProperties.put("listener.name.admin.sasl.enabled.mechanisms", "PLAIN");
        final String additionalPlainUsers = users.stream()
                .filter(Objects::nonNull)
                .filter(user -> AuthenticationType.PLAIN == user.getAuthenticationType())
                .map(user -> format("user_%1$s=\"%2$s\"", user.getUsername(), user.getPassword()))
                .reduce((one, two) -> one + " " + two).orElse("");
        brokerProperties.put("listener.name.admin.plain.sasl.jaas.config", String.format("org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%1$s\" password=\"%2$s\" user_%1$s=\"%2$s\" %3$s;", adminUser.getUsername(), adminUser.getPassword(), additionalPlainUsers));
        // Client SASL channel
        final String additionalScramUsers = users.stream()
                .filter(Objects::nonNull)
                .filter(user -> AuthenticationType.SCRAM == user.getAuthenticationType())
                .map(user -> format("user_%1$s=\"%2$s\"", user.getUsername(), user.getPassword()))
                .reduce((one, two) -> one + " " + two).orElse("");
        brokerProperties.put("listener.name.clientsasl.sasl.enabled.mechanisms", "SCRAM-SHA-256");
        brokerProperties.put("listener.name.clientsasl.scram-sha-256.sasl.jaas.config", String.format("org.apache.kafka.common.security.scram.ScramLoginModule required %s;", additionalScramUsers));
        brokerProperties.put("listener.name.clientsasl.scram-sha-256.sasl.server.callback.handler.class", ScramCallback.class.getName());

        brokerProperties.put("super.users", "User:" + adminUser.getUsername());

        return brokerProperties;
    }

    private String getResourcePath(String resource) {
        URL url = KafkaTestClusterImpl.class.getClassLoader().getResource(resource);
        if (url == null) {
            throw new RuntimeException("Could not find resource " + resource);
        }
        try {
            return Paths.get(url.toURI()).toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Could not create URI for resource " + resource);
        }
    }

    @Override
    public String bootstrapServers(AuthenticationType channel) {
        Objects.requireNonNull(channel);
        switch (channel) {
            case SSL:
                return bootstrapServers(brokerPortClientSsl);
            case SCRAM:
                return bootstrapServers(brokerPortClientSasl);
            case PLAIN:
                return bootstrapServers(brokerPortAdmin);
            default:
                throw new IllegalArgumentException("Unknown channel " + channel);
        }
    }

    private String bootstrapServers(int port) {
        return format("localhost:%d", port);
    }

    @Override
    public Map<String, Object> clientConnectionProperties(User user) {
        Map<String, Object> connectionProperties = new HashMap<>();
        connectionProperties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers(user.getAuthenticationType()));
        switch (user.getAuthenticationType()) {
            case PLAIN:
                connectionProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SASL_PLAINTEXT.name);
                connectionProperties.put(SaslConfigs.SASL_JAAS_CONFIG, String.format(
                        "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";",
                        user.getUsername(), user.getPassword()));
                connectionProperties.put(SaslConfigs.SASL_MECHANISM, "PLAIN");
                break;
            case SSL:
                connectionProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL.name);
                connectionProperties.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, user.getKeystoreLocation());
                connectionProperties.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, user.getKeystorePassword());
                connectionProperties.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, user.getKeyPassword());
                break;
            case SCRAM:
                connectionProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SASL_PLAINTEXT.name);
                connectionProperties.put(SaslConfigs.SASL_JAAS_CONFIG, String.format(
                        "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";",
                        user.getUsername(), user.getPassword()));
                connectionProperties.put(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-256");
                break;
        }

        if (user.getTruststoreLocation() != null && user.getTruststorePassword() != null && !user.getTruststoreLocation().trim().isEmpty()) {
            connectionProperties.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, user.getTruststoreLocation());
            connectionProperties.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, user.getTruststorePassword());
        }
        return connectionProperties;
    }

    @Override
    public Map<String, Object> adminProperties(User user) {
        return clientConnectionProperties(user);
    }

    @Override
    public void createTopic(String name, int partitions, boolean compacted) {
        LOG.info("Creating topic {} with {} partitions for KafkaTestCluster {}", name, partitions, clusterId);
        if (name == null) {
            throw new IllegalArgumentException("Topic name cannot be null");
        }

        try (AdminClient adminClient = AdminClient.create(adminProperties(adminUser))) {
            NewTopic newTopic = new NewTopic(name, partitions, REPLICATION_FACTOR);
            newTopic.configs(Collections.singletonMap(TopicConfig.CLEANUP_POLICY_CONFIG, compacted ? TopicConfig.CLEANUP_POLICY_COMPACT : TopicConfig.CLEANUP_POLICY_DELETE));

            CreateTopicsResult createResult = adminClient.createTopics(Collections.singleton(newTopic));
            try {
                createResult.all().get();
                LOG.info("Created topic {} with {} partitions for KafkaTestCluster {}", name, partitions, clusterId);
            } catch (InterruptedException e) {
                throw new RuntimeException("Create topic for topic " + name + " was interrupted", e);
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause instanceof TopicExistsException) {
                    LOG.info("Topic {} already exists", name);
                    return;
                }
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException) cause;
                }
                throw new RuntimeException("Create topic for topic " + name + " has failed", cause == null ? e : cause);
            }
        }
    }

    @Override
    public void deleteTopic(String name) {
        LOG.info("Deleting topic {} for KafkaTestCluster {}", name, clusterId);
        if (name == null) {
            throw new IllegalArgumentException("Topic name cannot be null");
        }

        try (AdminClient adminClient = AdminClient.create(adminProperties(adminUser))) {
            DeleteTopicsResult deleteResult = adminClient.deleteTopics(Collections.singleton(name));
            deleteResult.values().get(name).get();
        } catch (InterruptedException e) {
            throw new RuntimeException("Delete topic " + name + " was interrupted", e);
        } catch (ExecutionException e) {
            throw new RuntimeException("Delete topic " + name + " has failed", e.getCause() == null ? e : e.getCause());
        }
    }

    @Override
    public boolean makeTopicVisible(User user, String topic) {
        final ResourcePattern resourcePattern = new ResourcePattern(ResourceType.TOPIC, topic, PatternType.LITERAL);
        try (AdminClient adminClient = AdminClient.create(adminProperties(adminUser))) {
            adminClient.createAcls(Arrays.asList(
                    new AclBinding(resourcePattern, new AccessControlEntry("User:"+user.getUsername(), "*", AclOperation.WRITE, AclPermissionType.ALLOW)),
                    new AclBinding(resourcePattern, new AccessControlEntry("User:"+user.getUsername(), "*", AclOperation.IDEMPOTENT_WRITE, AclPermissionType.ALLOW)),
                    new AclBinding(resourcePattern, new AccessControlEntry("User:"+user.getUsername(), "*", AclOperation.DESCRIBE_CONFIGS, AclPermissionType.ALLOW)),
                    new AclBinding(resourcePattern, new AccessControlEntry("User:"+user.getUsername(), "*", AclOperation.DESCRIBE, AclPermissionType.ALLOW))
            )).all().get();
        } catch (ExecutionException e) {
            throw new RuntimeException("Could not create produce ACLs on topic " + topic + " for user " + user.getUsername(), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return false;
    }

    // NEEDED BECAUSE NO SUPPORT TO CREATE SCRAM USERS IN KAFKA 2.6 WITH BROKER PROPERTIES OR ADMINCLIENT
    public static class ScramCallback implements AuthenticateCallbackHandler {
        private static final String JAAS_USER_PREFIX = "user_";
        private static final SecureRandom random = new SecureRandom();
        private final Map<String, ScramCredential> credentialMap = new HashMap<>();

        @Override
        public void configure(Map<String, ?> configs, String saslMechanism, List<AppConfigurationEntry> jaasConfigEntries) {
            final ScramFormatter formatter;
            try {
                formatter = new ScramFormatter(ScramMechanism.forMechanismName(saslMechanism));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }

            for (AppConfigurationEntry configEntry : jaasConfigEntries) {
                for (String key : configEntry.getOptions().keySet()) {
                    if (key.startsWith(JAAS_USER_PREFIX)) {
                        final String username = key.substring(JAAS_USER_PREFIX.length());
                        final String password = (String) configEntry.getOptions().get(key);
                        final int iterations = random.nextInt(4096) + 4096;
                        credentialMap.put(username, formatter.generateCredential(password, iterations));
                    }
                }
            }
        }

        @Override
        public void close() {

        }

        @Override
        public void handle(Callback[] callbacks) throws UnsupportedCallbackException {
            String username = null;
            for (Callback callback : callbacks) {
                if (callback instanceof NameCallback) {
                    username = ((NameCallback) callback).getDefaultName();
                } else if (callback instanceof ScramCredentialCallback) {
                    ScramCredentialCallback sc = (ScramCredentialCallback) callback;
                    sc.scramCredential(credentialMap.get(username));
                } else
                    throw new UnsupportedCallbackException(callback);
            }
        }
    }
}
