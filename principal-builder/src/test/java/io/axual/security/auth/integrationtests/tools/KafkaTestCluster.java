package io.axual.security.auth.integrationtests.tools;

/*-
 * ========================LICENSE_START=================================
 * Axual Principal Builder for Kafka
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;
import java.util.UUID;

public interface KafkaTestCluster {
    String bootstrapServers(AuthenticationType channel);

    default int getDefaultTopicPartitions() {
        return 1;
    }

    default void createTopic(String name) {
        createTopic(name, getDefaultTopicPartitions());
    }

    default void createTopic(String name, int partitions) {
        createTopic(name, partitions, false);
    }

    void createTopic(String name, int partitions, boolean compacted);

    void deleteTopic(String name);

    Map<String, Object> clientConnectionProperties(User user);

    Map<String, Object> adminProperties(User user);

    boolean makeTopicVisible(User user, String topic);
}
