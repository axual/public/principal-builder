# Changelog
All notable changes to this project will be documented in this file.

## [Master]
### Changes

## [3.0.0] - 2022-06-27
### Changes
* [Issue #2](https://gitlab.com/axual/public/principal-builder/-/issues/2) -
  Update dependencies to Kafka 2.8.0
* [Issue #4](https://gitlab.com/axual/public/principal-builder/-/issues/4) -
    Update dependencies to Kafka 3.2.0

## [2.5.0] - 2021-08-23
### Changes
* [Issue #3](https://gitlab.com/axual/public/principal-builder/-/issues/3) -
  Add SASL Support

## [2.4.0] - 2020-10-14
### Changes
* [Issue #1](https://gitlab.com/axual/public/principal-builder/-/issues/1) -
  Update dependencies to Kafka 2.6.0

[Master]: https://gitlab.com/axual/public/principal-builder/-/compare/3.0.0...master
[3.0.0]: https://gitlab.com/axual/public/principal-builder/-/compare/2.5.0...3.0.0
[2.5.0]: https://gitlab.com/axual/public/principal-builder/-/compare/2.4.0...2.5.0
[2.4.0]: https://gitlab.com/axual/public/principal-builder/-/compare/2.3.0...2.4.0
